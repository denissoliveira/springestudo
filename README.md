# Estudo SpringBoot

Estudo de SpringBoot

## Começando

- Clonar o projeto em: git@gitlab.com:denissoliveira/springestudo.git ;

### Pré-requisitos

- Download do [Eclipse Spring tooling](https://spring.io/tools);
- NodeJS ([Standard-version](https://github.com/conventional-changelog/standard-version))

### Instalando

Recomendado instalar o [sdkman](https://sdkman.io/) que é uma ferramenta para gerenciar versões paralelas de vários kits de desenvolvimento de software na maioria dos sistemas baseados em Unix.

Com o sdkman, instale o java:

```sh
sdk list java
sdk install java 11.0.5.j9-adpt
```

Instalando o Maven:

```sh
sdk install maven
```

## Executando os testes

Explique como executar os testes automatizados para este sistema

### Divida em testes de ponta a ponta

```sh
mvn clean test -Ptest
```

### E testes de estilo de codificação

```sh
Dê um exemplo
```

## Desdobramento, desenvolvimento

Adicione notas adicionais sobre como implantar isso em um sistema ativo

## Construído com

- [Maven](https://maven.apache.org/) - Gerenciamento de dependências

## Contribuindo

Leia [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) para obter detalhes sobre nosso código de conduta e o processo para enviar solicitações pull para nós.

## Controle de versão

Usamos [SemVer](http://semver.org/) para versionar. Para as versões disponíveis, consulte as [tags neste repositório](https://github.com/your/project/tags).

## Autores

Veja também a lista de [colaboradores](https://github.com) que participaram deste projeto.

## Licença

Este projeto está licenciado sob a licença MIT - consulte o arquivo [LICENSE.md](LICENSE.md) para obter detalhes

## Agradecimentos

- Gorjeta para qualquer pessoa cujo código foi usado;
- Inspiração;
- etc;
